/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Areas;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class AreaRectangulo {
    
    public static void main(String[] args) {

        Scanner lector = new Scanner(System.in);
        double base = 0, altura = 0;

        System.out.println("AREA DEL RECTANGULO\n");

        System.out.println("Ingrese base ");
        base = lector.nextDouble();
        System.out.println("Ingrese altura ");
        altura = lector.nextDouble();

        System.out.println("El area del rectangulo es: " + (base * altura)
                + " Unidades^2");
    }
    
}
