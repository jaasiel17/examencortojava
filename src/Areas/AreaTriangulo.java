/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Areas;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class AreaTriangulo {

    public static void main(String[] args) {

        Scanner lector = new Scanner(System.in);
        double base = 0, altura = 0;

        System.out.println("AREA DEL TRIANGULO\n");

        System.out.println("Ingrese la base");
        base = lector.nextDouble();
        System.out.println("Ingrese la altura");
        altura = lector.nextDouble();

        System.out.println("El area del triangulo es: " + (base * altura) / 2
                + " Unidades^2");

    }

}
