/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Areas;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class AreaRombo {
    
    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        double ancho = 0, alto = 0;

        System.out.println("AREA DEL ROMBO\n");

        System.out.println("Ingrese ancho ");
        ancho = lector.nextDouble();
        System.out.println("Ingrese alto ");
        alto = lector.nextDouble();

        System.out.println("El area del rombo es: " + (ancho * alto)/2
                + " Unidades^2");
    }
    
}
