/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Areas;

import static java.lang.Math.PI;
import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class AreaCilindro {

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        double radio = 0, altura = 0;

        System.out.println("AREA DEL CILINDRO\n");

        System.out.println("Ingrese el radio ");
        radio = lector.nextDouble();
        System.out.println("Ingrese altura");
        altura = lector.nextDouble();

        System.out.println("El area del cilindro es: " + (2 * PI * radio) * 
                (radio + altura) + " Unidades^2");
    }

}
