/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Areas;

import java.util.Scanner;

/**
 *
 * @author Jaasiel Guerra
 */
public class AreaCuadrado {

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        double base = 0, baseDos = 0;

        System.out.println("AREA DEL CUADRADO\n");

        System.out.println("Ingrese lado a ");
        base = lector.nextDouble();
        System.out.println("Ingrese lado b");
        baseDos = lector.nextDouble();

        System.out.println("El area del cuadrado es: " + (base * baseDos)
                + " Unidades^2");
    }

}
